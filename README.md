# KUBERNETES B21

### To understand K8s
- What is Kubernetes?
- Why Kubernetes?
- Kubernetes Architecture
- KUbernetes Lifecycle
- Instalation of Minikube

### List of Objects
- Pod - Smallest entity managing by the k8s. Thin wrapper on container.
- Service - expose application in network
    - ClusterIP - Expose application inside the cluster
    - NodePort - Expose application outside the cluster with the help of Node Ports
    - LoadBalancer - Expose application outside the cluster using cloud loadbalancers
- Volume - mount on pods
    - persitent volume - will last after the pod dies
    - ephemeral volume - will last till pod running
- Namespaces - segregate the objects/resources
- Deployments - manage multiple pods and provides scaling, strategy, and rollback features.
- ReplicationController - It ensure that the correct number of pod replicas are running on the cluster. Uses equility-based selector. 
- ReplicaSet - It ensure that the correct number of pod replicas are running on the cluster. Uses in, notin, exists operator based selector. 
- DaemonSet - To create pod on all nodes / specific node similar to daemon
- StatefulSet - Will provide unique indentities to pods even it restarts
- ConfigMaps / Secrets - Use to store the credential





### Kubernetes Cheat Sheet

- `kubectl get nodes` - list all nodes present in the cluster
- `kubectl get pods` - list all pods running in the cluster
- `kubectl get pods -o wide` - to get ip of pods
- `kubectl run --image httpd web` - 
- `kubectl describe pod web` - detailed information of pods
- `kubectl get <objects>` - get list of objects
- `kubectl get all` - get list of all objects runnning in the container
- `kubectl get ns` - list namespaces
- `kubectl create namespace <name>` - create namespace
- `kubectl delete namespace <name>` - to delete namespace
- `kubectl get pods -n <namespace_name>` - to get pods in specific namespace. Use -n option to execute commond in perticular namespace 
- `kubectl describe pod <pod_name>` - to get detailed info about pod
- `kubectl logs <pod_name> -c <container_name>` - to get logs of the container
- `kubectl delete all --all` - to delete all objects


### Agendas:
- Manifest
- update K8s Cheat Sheet
- Kubernetes Commands
- readiness and liveness probe
